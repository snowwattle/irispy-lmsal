.. _getting_help:

************
Getting Help
************

If you are unsure of the general features of the irispy package, your first stop should be this guide which we strive to make as comprehensive, yet understandable, as possible.
If you would like to know more about how to use a specific function, class, etc., documentation for each object is stored in the source code itself.
It can be accessed there or in the :ref:`api` of this guide.
If you cannot find the answer to your issue there, would like to provide feedback, or would like help contributing code to irispy, you can get help directly from the irispy team in a number of ways.

TODO: Are there any ways?
