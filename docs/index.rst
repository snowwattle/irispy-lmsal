********************************
Welcome to the irispy user guide
********************************

Welcome to the irispy user guide.

irispy is a Python package designed for reading, manipulating and visualizing data taken with the Interface Region Imaging Spectrometer (IRIS).

.. warning::

    irispy is still under heavy development and has not yet seen its first release.
    The API can change at any time and so should not be relied upon.
    However, we are striving towards releasing a stable version.
    If you would like to help by providing user feedback, reporting bugs, or contributing code, install the development version as outlined in :ref:`dev_install`.

Contents
========

.. toctree::
   :maxdepth: 2

   introduction
   installation
   getting_help
   contributing
   code_ref/index
   whatsnew/index
