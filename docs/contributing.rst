**********************
Contributing to irispy
**********************

We are always enthusiastic to welcome new users and developers who want to enhance the irispy.
You can contribute in several ways, from providing feedback, reporting bugs, contributing code, and reviewing pull requests.
There is a role for almost any level of engagement.

Providing Feedback
==================

We could always use more voices and opinions in the discussions about irispy and its development from both users and developers.
There are a r of ways to make your voice heard.
Whether it be constructive criticism, inquiries about current or future capabilities, or flattering praise, we would love to hear from you.
See ref:`getting_help` for ways to get in touch.

Reporting Bugs
==============

If you run into unexpected behavior or a bug please report it.
All bugs are raised and stored on our `issue tracker`_.
If you are not sure if your problem is a bug, a deficiency in functionality, or something else, send us a message.
Ideally, we would like a short code example so we can run into the bug on our own machines.
See ref:`getting_help` for more information.

Contributing Code
=================

If you would like to contribute code, it is strongly recommended that you first discuss your aims with the irispy community.
We strive to be an open and welcoming community for developers of all experience levels.
Discussing your ideas before you start can give you new insights that will make your development easier, lead to a better end product, and reduce the chances of your work being regretfully rejected because of an issue you weren't aware of, e.g. the functionality already exists elsewhere.
See :ref:`getting_help` to contact the irispy community.

In the rest of this section we will go through the steps needed to set up your system so you can contribute code to irispy.
This is done using `git`_ version control software and `GitLab`_, a website that allows you to upload, update, and share code repositories (repos).
If you are new to code development or git and GitLab you can learn more from the following guides:

* `SunPy Newcomers Guide`_
* `GitLab guide`_
* `git guide`_
* `SunPy version control guide`_

The principles in the SunPy guides for contributing code and utilizing GitLab and git are exactly the same for irispy except that we contribute to the irispy repository rather than the SunPy one.
If you are a more seasoned developer and would like to get further information, you can check out the `SunPy Developers Guide`_.

Before you can contribute code to irispy, you first need to install the development version of irispy.
To find out how, see :ref:`dev_install`.

.. _issue tracker: https://gitlab.com/LMSAL_HUB/iris_hub/irispy-lmsal/issues
.. _SunPy Newcomers Guide: http://docs.sunpy.org/en/latest/dev_guide/newcomers.html
.. _GitLab: https://about.gitlab.com/
.. _git: https://git-scm.com/
.. _GitLab guide: https://docs.gitlab.com/ee/gitlab-basics/
.. _git guide: https://git-scm.com/book/en/v2/Getting-Started-Git-Basics
.. _SunPy version control guide: http://docs.sunpy.org/en/latest/dev_guide/version_control.html
.. _SunPy Developers Guide: http://docs.sunpy.org/en/latest/dev_guide
