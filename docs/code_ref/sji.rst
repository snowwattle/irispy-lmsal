irispy SJI
**********

The sji module provides tools to read and work with level 2 sji data.

.. automodapi:: irispy.sji
