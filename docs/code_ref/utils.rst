irispy utils
************

The utils module provides tools to work on irispy objects.

.. automodapi:: irispy.utils
