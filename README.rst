A Python package that provides the tools to read in and analyze data from the IRIS solar-observing satellite.
*************************************************************************************************************

The Interface Region Imaging Spectrograph is a NASA-funded Small Explorer which uses a high-frame-rate ultraviolet imaging spectrometer to make observations of the Sun.
It provides 0.3 arcsec angular resolution and sub-angstrom spectral resolution at a one second cadence.

For more information see the mission/instrument paper which is available `online for free <https://www.lmsal.com/iris_science/doc?cmd=dcur&proj_num=IS0196&file_type=pdf>`__.

.. image:: http://iris.lmsal.com/images/iris_full.jpg
  :width: 400
  :alt: Image of IRIS Spacecraft

License
*******

This project is Copyright (c) LMSAL and licensed under the terms of the BSD 3-Clause license.
This package is based upon the `Openastronomy packaging guide <https://github.com/OpenAstronomy/packaging-guide>`_ which is licensed under the BSD 3-clause licence.
See the licenses folder for more information.
